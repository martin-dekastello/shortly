defmodule Shortly.Repo.Migrations.CreateUrls do
  use Ecto.Migration

  def change do
    create table(:urls) do
      add :url, :string, size: 2200
      add :slug, :string
      add :slug_id, :integer
      add :site_id, references(:domains, on_delete: :nothing), null: false

      timestamps()
    end

    create index(:urls, [:site_id])
    create index(:urls, [:slug])

    create unique_index(:urls, [:slug, :site_id], name: :slug_site_id)
    create unique_index(:urls, [:slug_id, :site_id], name: :slug_id_site_id)
  end
end
