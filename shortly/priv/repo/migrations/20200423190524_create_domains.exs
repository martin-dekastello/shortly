defmodule Shortly.Repo.Migrations.CreateDomains do
  use Ecto.Migration

  def change do
    create table(:domains) do
      add :domain, :string
      add :is_public, :boolean, default: false, null: false

      timestamps()
    end
  end
end
