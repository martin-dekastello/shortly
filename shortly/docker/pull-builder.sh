#!/bin/bash

if [[ "$(docker images -q registry.gitlab.com/don-mums/phoenix-builder:"$VERSION" 2> /dev/null)" == "" ]]; then
  docker pull  registry.gitlab.com/don-mums/phoenix-builder:"$VERSION"
fi
