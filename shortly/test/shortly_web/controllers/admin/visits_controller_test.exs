defmodule ShortlyWeb.Admin.VisitsControllerTest do
  use ShortlyWeb.ConnCase

  alias Shortly.Chibi

  @create_attrs %{
    referrer: "some referrer",
    site_id: 42,
    url_id: 42,
    user_agent: "some user_agent"
  }
  @update_attrs %{
    referrer: "some updated referrer",
    site_id: 43,
    url_id: 43,
    user_agent: "some updated user_agent"
  }
  @invalid_attrs %{referrer: nil, site_id: nil, url_id: nil, user_agent: nil}

  def fixture(:visits) do
    {:ok, visits} = Chibi.create_visits(@create_attrs)
    visits
  end

  describe "index" do
    test "lists all visits", %{conn: conn} do
      conn = get(conn, Routes.admin_visits_path(conn, :index))
      assert html_response(conn, 200) =~ "Visits"
    end
  end

  describe "new visits" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.admin_visits_path(conn, :new))
      assert html_response(conn, 200) =~ "New Visits"
    end
  end

  describe "create visits" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, Routes.admin_visits_path(conn, :create), visits: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.admin_visits_path(conn, :show, id)

      conn = get(conn, Routes.admin_visits_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Visits Details"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, Routes.admin_visits_path(conn, :create), visits: @invalid_attrs
      assert html_response(conn, 200) =~ "New Visits"
    end
  end

  describe "edit visits" do
    setup [:create_visits]

    test "renders form for editing chosen visits", %{conn: conn, visits: visits} do
      conn = get(conn, Routes.admin_visits_path(conn, :edit, visits))
      assert html_response(conn, 200) =~ "Edit Visits"
    end
  end

  describe "update visits" do
    setup [:create_visits]

    test "redirects when data is valid", %{conn: conn, visits: visits} do
      conn = put conn, Routes.admin_visits_path(conn, :update, visits), visits: @update_attrs
      assert redirected_to(conn) == Routes.admin_visits_path(conn, :show, visits)

      conn = get(conn, Routes.admin_visits_path(conn, :show, visits))
      assert html_response(conn, 200) =~ "some updated referrer"
    end

    test "renders errors when data is invalid", %{conn: conn, visits: visits} do
      conn = put conn, Routes.admin_visits_path(conn, :update, visits), visits: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Visits"
    end
  end

  describe "delete visits" do
    setup [:create_visits]

    test "deletes chosen visits", %{conn: conn, visits: visits} do
      conn = delete(conn, Routes.admin_visits_path(conn, :delete, visits))
      assert redirected_to(conn) == Routes.admin_visits_path(conn, :index)

      assert_error_sent 404, fn ->
        get(conn, Routes.admin_visits_path(conn, :show, visits))
      end
    end
  end

  defp create_visits(_) do
    visits = fixture(:visits)
    {:ok, visits: visits}
  end
end
