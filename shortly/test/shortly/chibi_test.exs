defmodule Shortly.ChibiTest do
  use Shortly.DataCase

  alias Shortly.Chibi

  describe "urls" do
    alias Shortly.Chibi.Url

    @valid_attrs %{slug: "some slug", slug_id: 42, urls: "some urls"}
    @update_attrs %{slug: "some updated slug", slug_id: 43, urls: "some updated urls"}
    @invalid_attrs %{slug: nil, slug_id: nil, urls: nil}

    def url_fixture(attrs \\ %{}) do
      {:ok, url} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Chibi.create_url()

      url
    end

    test "list_urls/0 returns all urls" do
      url = url_fixture()
      assert Chibi.list_urls() == [url]
    end

    test "get_url!/1 returns the url with given id" do
      url = url_fixture()
      assert Chibi.get_url!(url.id) == url
    end

    test "create_url/1 with valid data creates a url" do
      assert {:ok, %Url{} = url} = Chibi.create_url(@valid_attrs)
      assert url.slug == "some slug"
      assert url.slug_id == 42
      assert url.urls == "some urls"
    end

    test "create_url/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Chibi.create_url(@invalid_attrs)
    end

    test "update_url/2 with valid data updates the url" do
      url = url_fixture()
      assert {:ok, %Url{} = url} = Chibi.update_url(url, @update_attrs)
      assert url.slug == "some updated slug"
      assert url.slug_id == 43
      assert url.urls == "some updated urls"
    end

    test "update_url/2 with invalid data returns error changeset" do
      url = url_fixture()
      assert {:error, %Ecto.Changeset{}} = Chibi.update_url(url, @invalid_attrs)
      assert url == Chibi.get_url!(url.id)
    end

    test "delete_url/1 deletes the url" do
      url = url_fixture()
      assert {:ok, %Url{}} = Chibi.delete_url(url)
      assert_raise Ecto.NoResultsError, fn -> Chibi.get_url!(url.id) end
    end

    test "change_url/1 returns a url changeset" do
      url = url_fixture()
      assert %Ecto.Changeset{} = Chibi.change_url(url)
    end
  end

  describe "visits" do
    alias Shortly.Chibi.Visit

    @valid_attrs %{referrer: "some referrer", user_agent: "some user_agent"}
    @update_attrs %{referrer: "some updated referrer", user_agent: "some updated user_agent"}
    @invalid_attrs %{referrer: nil, user_agent: nil}

    def visit_fixture(attrs \\ %{}) do
      {:ok, visit} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Chibi.create_visit()

      visit
    end

    test "list_visits/0 returns all visits" do
      visit = visit_fixture()
      assert Chibi.list_visits() == [visit]
    end

    test "get_visit!/1 returns the visit with given id" do
      visit = visit_fixture()
      assert Chibi.get_visit!(visit.id) == visit
    end

    test "create_visit/1 with valid data creates a visit" do
      assert {:ok, %Visit{} = visit} = Chibi.create_visit(@valid_attrs)
      assert visit.referrer == "some referrer"
      assert visit.user_agent == "some user_agent"
    end

    test "create_visit/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Chibi.create_visit(@invalid_attrs)
    end

    test "update_visit/2 with valid data updates the visit" do
      visit = visit_fixture()
      assert {:ok, %Visit{} = visit} = Chibi.update_visit(visit, @update_attrs)
      assert visit.referrer == "some updated referrer"
      assert visit.user_agent == "some updated user_agent"
    end

    test "update_visit/2 with invalid data returns error changeset" do
      visit = visit_fixture()
      assert {:error, %Ecto.Changeset{}} = Chibi.update_visit(visit, @invalid_attrs)
      assert visit == Chibi.get_visit!(visit.id)
    end

    test "delete_visit/1 deletes the visit" do
      visit = visit_fixture()
      assert {:ok, %Visit{}} = Chibi.delete_visit(visit)
      assert_raise Ecto.NoResultsError, fn -> Chibi.get_visit!(visit.id) end
    end

    test "change_visit/1 returns a visit changeset" do
      visit = visit_fixture()
      assert %Ecto.Changeset{} = Chibi.change_visit(visit)
    end
  end

  describe "urls" do
    alias Shortly.Chibi.Url

    @valid_attrs %{slug: "some slug", slug_id: 42, url: "some url"}
    @update_attrs %{slug: "some updated slug", slug_id: 43, url: "some updated url"}
    @invalid_attrs %{slug: nil, slug_id: nil, url: nil}

    def url_fixture(attrs \\ %{}) do
      {:ok, url} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Chibi.create_url()

      url
    end

    test "paginate_urls/1 returns paginated list of urls" do
      for _ <- 1..20 do
        url_fixture()
      end

      {:ok, %{urls: urls} = page} = Chibi.paginate_urls(%{})

      assert length(urls) == 15
      assert page.page_number == 1
      assert page.page_size == 15
      assert page.total_pages == 2
      assert page.total_entries == 20
      assert page.distance == 5
      assert page.sort_field == "inserted_at"
      assert page.sort_direction == "desc"
    end

    test "list_urls/0 returns all urls" do
      url = url_fixture()
      assert Chibi.list_urls() == [url]
    end

    test "get_url!/1 returns the url with given id" do
      url = url_fixture()
      assert Chibi.get_url!(url.id) == url
    end

    test "create_url/1 with valid data creates a url" do
      assert {:ok, %Url{} = url} = Chibi.create_url(@valid_attrs)
      assert url.slug == "some slug"
      assert url.slug_id == 42
      assert url.url == "some url"
    end

    test "create_url/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Chibi.create_url(@invalid_attrs)
    end

    test "update_url/2 with valid data updates the url" do
      url = url_fixture()
      assert {:ok, url} = Chibi.update_url(url, @update_attrs)
      assert %Url{} = url
      assert url.slug == "some updated slug"
      assert url.slug_id == 43
      assert url.url == "some updated url"
    end

    test "update_url/2 with invalid data returns error changeset" do
      url = url_fixture()
      assert {:error, %Ecto.Changeset{}} = Chibi.update_url(url, @invalid_attrs)
      assert url == Chibi.get_url!(url.id)
    end

    test "delete_url/1 deletes the url" do
      url = url_fixture()
      assert {:ok, %Url{}} = Chibi.delete_url(url)
      assert_raise Ecto.NoResultsError, fn -> Chibi.get_url!(url.id) end
    end

    test "change_url/1 returns a url changeset" do
      url = url_fixture()
      assert %Ecto.Changeset{} = Chibi.change_url(url)
    end
  end

  describe "visits" do
    alias Shortly.Chibi.Visits

    @valid_attrs %{
      referrer: "some referrer",
      site_id: 42,
      url_id: 42,
      user_agent: "some user_agent"
    }
    @update_attrs %{
      referrer: "some updated referrer",
      site_id: 43,
      url_id: 43,
      user_agent: "some updated user_agent"
    }
    @invalid_attrs %{referrer: nil, site_id: nil, url_id: nil, user_agent: nil}

    def visits_fixture(attrs \\ %{}) do
      {:ok, visits} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Chibi.create_visits()

      visits
    end

    test "paginate_visits/1 returns paginated list of visits" do
      for _ <- 1..20 do
        visits_fixture()
      end

      {:ok, %{visits: visits} = page} = Chibi.paginate_visits(%{})

      assert length(visits) == 15
      assert page.page_number == 1
      assert page.page_size == 15
      assert page.total_pages == 2
      assert page.total_entries == 20
      assert page.distance == 5
      assert page.sort_field == "inserted_at"
      assert page.sort_direction == "desc"
    end

    test "list_visits/0 returns all visits" do
      visits = visits_fixture()
      assert Chibi.list_visits() == [visits]
    end

    test "get_visits!/1 returns the visits with given id" do
      visits = visits_fixture()
      assert Chibi.get_visits!(visits.id) == visits
    end

    test "create_visits/1 with valid data creates a visits" do
      assert {:ok, %Visits{} = visits} = Chibi.create_visits(@valid_attrs)
      assert visits.referrer == "some referrer"
      assert visits.site_id == 42
      assert visits.url_id == 42
      assert visits.user_agent == "some user_agent"
    end

    test "create_visits/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Chibi.create_visits(@invalid_attrs)
    end

    test "update_visits/2 with valid data updates the visits" do
      visits = visits_fixture()
      assert {:ok, visits} = Chibi.update_visits(visits, @update_attrs)
      assert %Visits{} = visits
      assert visits.referrer == "some updated referrer"
      assert visits.site_id == 43
      assert visits.url_id == 43
      assert visits.user_agent == "some updated user_agent"
    end

    test "update_visits/2 with invalid data returns error changeset" do
      visits = visits_fixture()
      assert {:error, %Ecto.Changeset{}} = Chibi.update_visits(visits, @invalid_attrs)
      assert visits == Chibi.get_visits!(visits.id)
    end

    test "delete_visits/1 deletes the visits" do
      visits = visits_fixture()
      assert {:ok, %Visits{}} = Chibi.delete_visits(visits)
      assert_raise Ecto.NoResultsError, fn -> Chibi.get_visits!(visits.id) end
    end

    test "change_visits/1 returns a visits changeset" do
      visits = visits_fixture()
      assert %Ecto.Changeset{} = Chibi.change_visits(visits)
    end
  end

  describe "visits" do
    alias Shortly.Chibi.Visit

    @valid_attrs %{
      referrer: "some referrer",
      site_id: 42,
      url_id: 42,
      user_agent: "some user_agent"
    }
    @update_attrs %{
      referrer: "some updated referrer",
      site_id: 43,
      url_id: 43,
      user_agent: "some updated user_agent"
    }
    @invalid_attrs %{referrer: nil, site_id: nil, url_id: nil, user_agent: nil}

    def visit_fixture(attrs \\ %{}) do
      {:ok, visit} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Chibi.create_visit()

      visit
    end

    test "paginate_visits/1 returns paginated list of visits" do
      for _ <- 1..20 do
        visit_fixture()
      end

      {:ok, %{visits: visits} = page} = Chibi.paginate_visits(%{})

      assert length(visits) == 15
      assert page.page_number == 1
      assert page.page_size == 15
      assert page.total_pages == 2
      assert page.total_entries == 20
      assert page.distance == 5
      assert page.sort_field == "inserted_at"
      assert page.sort_direction == "desc"
    end

    test "list_visits/0 returns all visits" do
      visit = visit_fixture()
      assert Chibi.list_visits() == [visit]
    end

    test "get_visit!/1 returns the visit with given id" do
      visit = visit_fixture()
      assert Chibi.get_visit!(visit.id) == visit
    end

    test "create_visit/1 with valid data creates a visit" do
      assert {:ok, %Visit{} = visit} = Chibi.create_visit(@valid_attrs)
      assert visit.referrer == "some referrer"
      assert visit.site_id == 42
      assert visit.url_id == 42
      assert visit.user_agent == "some user_agent"
    end

    test "create_visit/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Chibi.create_visit(@invalid_attrs)
    end

    test "update_visit/2 with valid data updates the visit" do
      visit = visit_fixture()
      assert {:ok, visit} = Chibi.update_visit(visit, @update_attrs)
      assert %Visit{} = visit
      assert visit.referrer == "some updated referrer"
      assert visit.site_id == 43
      assert visit.url_id == 43
      assert visit.user_agent == "some updated user_agent"
    end

    test "update_visit/2 with invalid data returns error changeset" do
      visit = visit_fixture()
      assert {:error, %Ecto.Changeset{}} = Chibi.update_visit(visit, @invalid_attrs)
      assert visit == Chibi.get_visit!(visit.id)
    end

    test "delete_visit/1 deletes the visit" do
      visit = visit_fixture()
      assert {:ok, %Visit{}} = Chibi.delete_visit(visit)
      assert_raise Ecto.NoResultsError, fn -> Chibi.get_visit!(visit.id) end
    end

    test "change_visit/1 returns a visit changeset" do
      visit = visit_fixture()
      assert %Ecto.Changeset{} = Chibi.change_visit(visit)
    end
  end
end
