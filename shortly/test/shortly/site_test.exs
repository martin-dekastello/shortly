defmodule Shortly.SiteTest do
  use Shortly.DataCase

  alias Shortly.Site

  describe "domains" do
    alias Shortly.Site.Domain

    @valid_attrs %{domain: "some domain", is_public: true}
    @update_attrs %{domain: "some updated domain", is_public: false}
    @invalid_attrs %{domain: nil, is_public: nil}

    def domain_fixture(attrs \\ %{}) do
      {:ok, domain} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Site.create_domain()

      domain
    end

    test "list_domains/0 returns all domains" do
      domain = domain_fixture()
      assert Site.list_domains() == [domain]
    end

    test "get_domain!/1 returns the domain with given id" do
      domain = domain_fixture()
      assert Site.get_domain!(domain.id) == domain
    end

    test "create_domain/1 with valid data creates a domain" do
      assert {:ok, %Domain{} = domain} = Site.create_domain(@valid_attrs)
      assert domain.domain == "some domain"
      assert domain.is_public == true
    end

    test "create_domain/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Site.create_domain(@invalid_attrs)
    end

    test "update_domain/2 with valid data updates the domain" do
      domain = domain_fixture()
      assert {:ok, %Domain{} = domain} = Site.update_domain(domain, @update_attrs)
      assert domain.domain == "some updated domain"
      assert domain.is_public == false
    end

    test "update_domain/2 with invalid data returns error changeset" do
      domain = domain_fixture()
      assert {:error, %Ecto.Changeset{}} = Site.update_domain(domain, @invalid_attrs)
      assert domain == Site.get_domain!(domain.id)
    end

    test "delete_domain/1 deletes the domain" do
      domain = domain_fixture()
      assert {:ok, %Domain{}} = Site.delete_domain(domain)
      assert_raise Ecto.NoResultsError, fn -> Site.get_domain!(domain.id) end
    end

    test "change_domain/1 returns a domain changeset" do
      domain = domain_fixture()
      assert %Ecto.Changeset{} = Site.change_domain(domain)
    end
  end

  describe "domains" do
    alias Shortly.Site.Domain

    @valid_attrs %{
      favicon_path: "some favicon_path",
      host: "some host",
      is_public: true,
      template: "some template",
      title: "some title",
      url: "some url"
    }
    @update_attrs %{
      favicon_path: "some updated favicon_path",
      host: "some updated host",
      is_public: false,
      template: "some updated template",
      title: "some updated title",
      url: "some updated url"
    }
    @invalid_attrs %{
      favicon_path: nil,
      host: nil,
      is_public: nil,
      template: nil,
      title: nil,
      url: nil
    }

    def domain_fixture(attrs \\ %{}) do
      {:ok, domain} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Site.create_domain()

      domain
    end

    test "paginate_domains/1 returns paginated list of domains" do
      for _ <- 1..20 do
        domain_fixture()
      end

      {:ok, %{domains: domains} = page} = Site.paginate_domains(%{})

      assert length(domains) == 15
      assert page.page_number == 1
      assert page.page_size == 15
      assert page.total_pages == 2
      assert page.total_entries == 20
      assert page.distance == 5
      assert page.sort_field == "inserted_at"
      assert page.sort_direction == "desc"
    end

    test "list_domains/0 returns all domains" do
      domain = domain_fixture()
      assert Site.list_domains() == [domain]
    end

    test "get_domain!/1 returns the domain with given id" do
      domain = domain_fixture()
      assert Site.get_domain!(domain.id) == domain
    end

    test "create_domain/1 with valid data creates a domain" do
      assert {:ok, %Domain{} = domain} = Site.create_domain(@valid_attrs)
      assert domain.favicon_path == "some favicon_path"
      assert domain.host == "some host"
      assert domain.is_public == true
      assert domain.template == "some template"
      assert domain.title == "some title"
      assert domain.url == "some url"
    end

    test "create_domain/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Site.create_domain(@invalid_attrs)
    end

    test "update_domain/2 with valid data updates the domain" do
      domain = domain_fixture()
      assert {:ok, domain} = Site.update_domain(domain, @update_attrs)
      assert %Domain{} = domain
      assert domain.favicon_path == "some updated favicon_path"
      assert domain.host == "some updated host"
      assert domain.is_public == false
      assert domain.template == "some updated template"
      assert domain.title == "some updated title"
      assert domain.url == "some updated url"
    end

    test "update_domain/2 with invalid data returns error changeset" do
      domain = domain_fixture()
      assert {:error, %Ecto.Changeset{}} = Site.update_domain(domain, @invalid_attrs)
      assert domain == Site.get_domain!(domain.id)
    end

    test "delete_domain/1 deletes the domain" do
      domain = domain_fixture()
      assert {:ok, %Domain{}} = Site.delete_domain(domain)
      assert_raise Ecto.NoResultsError, fn -> Site.get_domain!(domain.id) end
    end

    test "change_domain/1 returns a domain changeset" do
      domain = domain_fixture()
      assert %Ecto.Changeset{} = Site.change_domain(domain)
    end
  end

  describe "ips" do
    alias Shortly.Site.Ip

    @valid_attrs %{ip_address: "some ip_address", is_active: true}
    @update_attrs %{ip_address: "some updated ip_address", is_active: false}
    @invalid_attrs %{ip_address: nil, is_active: nil}

    def ip_fixture(attrs \\ %{}) do
      {:ok, ip} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Site.create_ip()

      ip
    end

    test "paginate_ips/1 returns paginated list of ips" do
      for _ <- 1..20 do
        ip_fixture()
      end

      {:ok, %{ips: ips} = page} = Site.paginate_ips(%{})

      assert length(ips) == 15
      assert page.page_number == 1
      assert page.page_size == 15
      assert page.total_pages == 2
      assert page.total_entries == 20
      assert page.distance == 5
      assert page.sort_field == "inserted_at"
      assert page.sort_direction == "desc"
    end

    test "list_ips/0 returns all ips" do
      ip = ip_fixture()
      assert Site.list_ips() == [ip]
    end

    test "get_ip!/1 returns the ip with given id" do
      ip = ip_fixture()
      assert Site.get_ip!(ip.id) == ip
    end

    test "create_ip/1 with valid data creates a ip" do
      assert {:ok, %Ip{} = ip} = Site.create_ip(@valid_attrs)
      assert ip.ip_address == "some ip_address"
      assert ip.is_active == true
    end

    test "create_ip/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Site.create_ip(@invalid_attrs)
    end

    test "update_ip/2 with valid data updates the ip" do
      ip = ip_fixture()
      assert {:ok, ip} = Site.update_ip(ip, @update_attrs)
      assert %Ip{} = ip
      assert ip.ip_address == "some updated ip_address"
      assert ip.is_active == false
    end

    test "update_ip/2 with invalid data returns error changeset" do
      ip = ip_fixture()
      assert {:error, %Ecto.Changeset{}} = Site.update_ip(ip, @invalid_attrs)
      assert ip == Site.get_ip!(ip.id)
    end

    test "delete_ip/1 deletes the ip" do
      ip = ip_fixture()
      assert {:ok, %Ip{}} = Site.delete_ip(ip)
      assert_raise Ecto.NoResultsError, fn -> Site.get_ip!(ip.id) end
    end

    test "change_ip/1 returns a ip changeset" do
      ip = ip_fixture()
      assert %Ecto.Changeset{} = Site.change_ip(ip)
    end
  end
end
