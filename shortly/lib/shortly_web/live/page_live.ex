defmodule ShortlyWeb.PageLive do
  use ShortlyWeb, :live_view
  alias Shortly.Chibi
  alias Shortly.Site
  alias Phoenix.PubSub

  @impl true
  def mount(_params, session, socket) do
    time_now = NaiveDateTime.utc_now()

    {:ok,
     assign(socket,
       input_url: "",
       long_url: "",
       short_url: "",
       slug: "",
       num_redirects: "",
       page_title: "",
       year: time_now.year,
       remote_ip: session["remote_ip"]
     )}
  end

  @impl true
  def handle_params(_params, url, socket) do
    host = Regex.replace(~r/\.$/, URI.parse(url).host, "")

    domain = Site.get_by_host!(host)
    scheme = URI.parse(url).scheme
    num_redirects = Chibi.get_num_redirects(domain.id)
    PubSub.subscribe(Shortly.PubSub, ShortlyWeb.Utils.redirects_topic(host))

    {:noreply,
     assign(socket,
       domain: domain,
       scheme: scheme,
       num_redirects: num_redirects,
       favicon_path: domain.favicon_path,
       page_title: domain.title
     )}
  end

  @impl true
  def handle_info(%{num_redirects: num_redirects}, socket) do
    {:noreply, assign(socket, num_redirects: num_redirects)}
  end

  @impl true
  def handle_event("shorten", %{"url" => input_url, "slug" => _slug}, socket)
      when input_url == "" do
    {:noreply, put_flash(socket, :error, "Url must not be empty.")}
  end

  @impl true
  def handle_event("shorten", %{"url" => input_url, "slug" => slug}, socket)
      when input_url != "" do
    string_ip = Enum.join(Tuple.to_list(socket.assigns.remote_ip))

    case Hammer.check_rate("shorten_url:#{string_ip}", 3 * 60 * 60_000, 20) do
      {:allow, _count} ->
        domain = socket.assigns.domain

        long_url =
          case URI.parse(input_url) do
            %{path: path, host: nil} ->
              "http://#{path}"

            _ ->
              input_url
          end

        create_short_url(domain, long_url, slug, socket)

      {:deny, _limit} ->
        {:noreply, put_flash(socket, :error, "You tried too many times please come back later.")}
    end
  end

  defp create_short_url(domain, long_url, slug, socket) do
    case Chibi.create_url(domain.id, %{"url" => long_url, "slug" => slug}) do
      {:ok, url_object} ->
        new_socket = put_flash(socket, :info, "Url created successfully.")

        {:noreply,
         assign(new_socket,
           long_url: url_object.url,
           short_url: "#{socket.assigns.scheme}://#{domain.url}/#{url_object.slug}"
         )}

      {:error, %Ecto.Changeset{} = changeset} ->
        new_socket =
          case changeset do
            %{errors: [slug: {message, _}]} ->
              put_flash(socket, :error, message)

            _ ->
              put_flash(socket, :error, "Unexpected error")
          end

        {:noreply, new_socket}
    end
  end

  @impl true
  def render(assigns) do
    Phoenix.View.render(ShortlyWeb.PageLiveView, "#{assigns.domain.template}.html", assigns)
  end
end
