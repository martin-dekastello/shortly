defmodule ShortlyWeb.Admin.UrlView do
  use ShortlyWeb, :view

  import Torch.TableView
  import Torch.FilterView
end
