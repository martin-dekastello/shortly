defmodule ShortlyWeb.Admin.UrlController do
  use ShortlyWeb, :controller

  alias Shortly.Chibi
  alias Shortly.Chibi.Url

  plug(:put_layout, {ShortlyWeb.LayoutView, "torch.html"})

  def index(conn, params) do
    case Chibi.paginate_urls(params) do
      {:ok, assigns} ->
        render(conn, "index.html", assigns)

      error ->
        conn
        |> put_flash(:error, "There was an error rendering Urls. #{inspect(error)}")
        |> redirect(to: Routes.admin_url_path(conn, :index))
    end
  end

  def new(conn, _params) do
    changeset = Chibi.change_url(%Url{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"url" => url_params}) do
    case Chibi.create_url(url_params) do
      {:ok, url} ->
        conn
        |> put_flash(:info, "Url created successfully.")
        |> redirect(to: Routes.admin_url_path(conn, :show, url))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    url = Chibi.get_url!(id)
    render(conn, "show.html", url: url)
  end

  def edit(conn, %{"id" => id}) do
    url = Chibi.get_url!(id)
    changeset = Chibi.change_url(url)
    render(conn, "edit.html", url: url, changeset: changeset)
  end

  def update(conn, %{"id" => id, "url" => url_params}) do
    url = Chibi.get_url!(id)

    case Chibi.update_url(url, url_params) do
      {:ok, url} ->
        conn
        |> put_flash(:info, "Url updated successfully.")
        |> redirect(to: Routes.admin_url_path(conn, :show, url))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", url: url, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    url = Chibi.get_url!(id)

    Chibi.delete_visits(%{url_id: id})
    {:ok, _url} = Chibi.delete_url(url)

    conn
    |> put_flash(:info, "Url deleted successfully.")
    |> redirect(to: Routes.admin_url_path(conn, :index))
  end
end
