defmodule ShortlyWeb.Admin.DomainController do
  use ShortlyWeb, :controller

  alias Shortly.Site
  alias Shortly.Site.Domain

  plug(:put_layout, {ShortlyWeb.LayoutView, "torch.html"})

  def index(conn, params) do
    case Site.paginate_domains(params) do
      {:ok, assigns} ->
        render(conn, "index.html", assigns)

      error ->
        conn
        |> put_flash(:error, "There was an error rendering Domains. #{inspect(error)}")
        |> redirect(to: Routes.admin_domain_path(conn, :index))
    end
  end

  def new(conn, _params) do
    changeset = Site.change_domain(%Domain{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"domain" => domain_params}) do
    case Site.create_domain(domain_params) do
      {:ok, domain} ->
        conn
        |> put_flash(:info, "Domain created successfully.")
        |> redirect(to: Routes.admin_domain_path(conn, :show, domain))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    domain = Site.get_domain!(id)
    render(conn, "show.html", domain: domain)
  end

  def edit(conn, %{"id" => id}) do
    domain = Site.get_domain!(id)
    changeset = Site.change_domain(domain)
    render(conn, "edit.html", domain: domain, changeset: changeset)
  end

  def update(conn, %{"id" => id, "domain" => domain_params}) do
    domain = Site.get_domain!(id)

    case Site.update_domain(domain, domain_params) do
      {:ok, domain} ->
        conn
        |> put_flash(:info, "Domain updated successfully.")
        |> redirect(to: Routes.admin_domain_path(conn, :show, domain))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", domain: domain, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    domain = Site.get_domain!(id)
    {:ok, _domain} = Site.delete_domain(domain)

    conn
    |> put_flash(:info, "Domain deleted successfully.")
    |> redirect(to: Routes.admin_domain_path(conn, :index))
  end
end
