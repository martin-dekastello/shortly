defmodule Shortly.Chibi.Url do
  use Ecto.Schema
  import Ecto.Changeset

  schema "urls" do
    field :slug, :string
    field :slug_id, :integer
    field :url, :string
    has_many :visits, Shortly.Chibi.Visit
    belongs_to :site, Shortly.Site.Domain

    timestamps()
  end

  @doc false
  def changeset(url, attrs) do
    url
    |> cast(attrs, [:url, :slug, :slug_id, :site_id])
    |> validate_required([:site_id, :slug])
    |> unsafe_validate_unique([:slug, :site_id], Shortly.Repo,
      message: "Alias is already used, please use something else than \"#{attrs["slug"]}\""
    )
  end
end
