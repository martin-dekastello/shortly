defmodule Shortly.Chibi.Visit do
  use Ecto.Schema
  import Ecto.Changeset

  schema "visits" do
    field :referrer, :string
    field :user_agent, :string
    belongs_to :url, Shortly.Chibi.Url
    belongs_to :site, Shortly.Site.Domain

    timestamps()
  end

  @doc false
  def changeset(visit, attrs) do
    visit
    |> cast(attrs, [:referrer, :user_agent])
    |> validate_required([:user_agent])
  end
end
