defmodule Shortly.Chibi do
  @moduledoc """
  The Chibi context.
  """

  import Torch.Helpers, only: [sort: 1, paginate: 4]
  import Filtrex.Type.Config
  import Ecto.Query, warn: false
  alias Shortly.{Repo, DbUtils}

  alias Shortly.Chibi.Visit
  alias Shortly.Chibi.Url

  @pagination [page_size: 15]
  @pagination_distance 5
  @doc """
  Returns the list of urls.

  ## Examples

      iex> list_urls()
      [%Url{}, ...]

  """
  def list_urls do
    Repo.all(Url)
  end

  @doc """
  Gets a single url.

  Raises `Ecto.NoResultsError` if the Url does not exist.

  ## Examples

      iex> get_url!(123)
      %Url{}

      iex> get_url!(456)
      ** (Ecto.NoResultsError)

  """
  def get_url!(id), do: Repo.get!(Url, id)

  def get_by_slug!(slug, site_id),
    do: Repo.one(from u in Url, where: u.slug == ^slug and u.site_id == ^site_id)

  @doc """
  Creates a url.

  ## Examples

      iex> create_url(domain, %{field: value})
      {:ok, %Url{}}

      iex> create_url(domain, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_url(site_id, attrs \\ %{}) do
    processed_attrs = get_slug(attrs, site_id)

    %Url{}
    |> Url.changeset(processed_attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a url.

  ## Examples

      iex> update_url(url, %{field: new_value})
      {:ok, %Url{}}

      iex> update_url(url, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_url(%Url{} = url, attrs) do
    url
    |> Url.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a url.

  ## Examples

      iex> delete_url(url)
      {:ok, %Url{}}

      iex> delete_url(url)
      {:error, %Ecto.Changeset{}}

  """
  def delete_url(%Url{} = url) do
    Repo.delete(url)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking url changes.

  ## Examples

      iex> change_url(url)
      %Ecto.Changeset{data: %Url{}}

  """
  def change_url(%Url{} = url, attrs \\ %{}) do
    Url.changeset(url, attrs)
  end

  @doc """
  Returns the list of visits.

  ## Examples

      iex> list_visits()
      [%Visit{}, ...]

  """
  def list_visits do
    Repo.all(Visit)
  end

  def get_num_redirects(site_id) do
    Repo.one(from v in Visit, select: count(v.id), where: v.site_id == ^site_id)
  end

  @doc """
  Gets a single visit.

  Raises `Ecto.NoResultsError` if the Visit does not exist.

  ## Examples

      iex> get_visit!(123)
      %Visit{}

      iex> get_visit!(456)
      ** (Ecto.NoResultsError)

  """
  def get_visit!(id), do: Repo.get!(Visit, id)

  @doc """
  Creates a visit.

  ## Examples

      iex> create_visit(url, %{field: value})
      {:ok, %Visit{}}

      iex> create_visit(url, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_visit(%Url{} = url, attrs \\ %{}) do
    %Visit{}
    |> Visit.changeset(attrs)
    |> Ecto.Changeset.put_change(:url_id, url.id)
    |> Ecto.Changeset.put_change(:site_id, url.site_id)
    |> Repo.insert()
  end

  @doc """
  Updates a visit.

  ## Examples

      iex> update_visit(visit, %{field: new_value})
      {:ok, %Visit{}}

      iex> update_visit(visit, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_visit(%Visit{} = visit, attrs) do
    visit
    |> Visit.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a visit.

  ## Examples

      iex> delete_visit(visit)
      {:ok, %Visit{}}

      iex> delete_visit(visit)
      {:error, %Ecto.Changeset{}}

  """
  def delete_visit(%Visit{} = visit) do
    Repo.delete(visit)
  end

  def delete_visits(filter_params) do
    from(t in Visit, select: t)
    |> DbUtils.filter_db(filter_params)
    |> Repo.delete_all()
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking visit changes.

  ## Examples

      iex> change_visit(visit)
      %Ecto.Changeset{data: %Visit{}}

  """
  def change_visit(%Visit{} = visit, attrs \\ %{}) do
    Visit.changeset(visit, attrs)
  end

  @slug_map "1aqzxsw23edcvfr45tgbnhy67ujmki89olp0QAZXRSWEDCVFTGBNHYUJMKIOLP"
  @slug_map_length 62

  defp add_slug_letter(slug_id) when slug_id <= @slug_map_length do
    reminder = rem(slug_id, @slug_map_length)
    String.slice(@slug_map, reminder..reminder)
  end

  defp add_slug_letter(slug_id) when slug_id > @slug_map_length do
    reminder = rem(slug_id, @slug_map_length)
    output = String.slice(@slug_map, reminder..reminder)

    output <> add_slug_letter(div(slug_id, @slug_map_length))
  end

  defp get_unique_slug(slug_id, site_id) do
    slug = add_slug_letter(slug_id)

    case Repo.one(from u in Url, where: u.slug == ^slug and u.site_id == ^site_id) do
      nil ->
        %{"slug_id" => slug_id, "slug" => slug, "site_id" => site_id}

      _url ->
        get_unique_slug(slug_id + 1, site_id)
    end
  end

  defp get_slug(attrs, site_id) do
    if attrs["slug"] == "" do
      slug_id =
        Repo.one(from u in Url, select: coalesce(max(u.slug_id), 0), where: u.site_id == ^site_id) +
          1

      slug_data = get_unique_slug(slug_id, site_id)

      Map.merge(attrs, slug_data)
    else
      Map.merge(attrs, %{"site_id" => site_id})
    end
  end

  @doc """
  Paginate the list of urls using filtrex
  filters.

  ## Examples

      iex> list_urls(%{})
      %{urls: [%Url{}], ...}
  """
  @spec paginate_urls(map) :: {:ok, map} | {:error, any}
  def paginate_urls(params \\ %{}) do
    params =
      params
      |> Map.put_new("sort_direction", "desc")
      |> Map.put_new("sort_field", "inserted_at")

    {:ok, sort_direction} = Map.fetch(params, "sort_direction")
    {:ok, sort_field} = Map.fetch(params, "sort_field")

    with {:ok, filter} <- Filtrex.parse_params(filter_config(:urls), params["url"] || %{}),
         %Scrivener.Page{} = page <- do_paginate_urls(filter, params) do
      {:ok,
       %{
         urls: page.entries,
         page_number: page.page_number,
         page_size: page.page_size,
         total_pages: page.total_pages,
         total_entries: page.total_entries,
         distance: @pagination_distance,
         sort_field: sort_field,
         sort_direction: sort_direction
       }}
    else
      {:error, error} -> {:error, error}
      error -> {:error, error}
    end
  end

  defp do_paginate_urls(filter, params) do
    Url
    |> Filtrex.query(filter)
    |> order_by(^sort(params))
    |> paginate(Repo, params, @pagination)
  end

  defp filter_config(:urls) do
    defconfig do
      text(:url)
      text(:slug)
      number(:slug_id)
    end
  end

  defp filter_config(:visits) do
    defconfig do
      text(:referrer)
      text(:user_agent)
      number(:url_id)
      number(:site_id)
    end
  end

  @doc """
  Paginate the list of visits using filtrex
  filters.

  ## Examples

      iex> list_visits(%{})
      %{visits: [%Visit{}], ...}
  """
  @spec paginate_visits(map) :: {:ok, map} | {:error, any}
  def paginate_visits(params \\ %{}) do
    params =
      params
      |> Map.put_new("sort_direction", "desc")
      |> Map.put_new("sort_field", "inserted_at")

    {:ok, sort_direction} = Map.fetch(params, "sort_direction")
    {:ok, sort_field} = Map.fetch(params, "sort_field")

    with {:ok, filter} <- Filtrex.parse_params(filter_config(:visits), params["visit"] || %{}),
         %Scrivener.Page{} = page <- do_paginate_visits(filter, params) do
      {:ok,
       %{
         visits: page.entries,
         page_number: page.page_number,
         page_size: page.page_size,
         total_pages: page.total_pages,
         total_entries: page.total_entries,
         distance: @pagination_distance,
         sort_field: sort_field,
         sort_direction: sort_direction
       }}
    else
      {:error, error} -> {:error, error}
      error -> {:error, error}
    end
  end

  defp do_paginate_visits(filter, params) do
    Visit
    |> Filtrex.query(filter)
    |> order_by(^sort(params))
    |> paginate(Repo, params, @pagination)
  end
end
