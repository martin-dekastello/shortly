## 1.1.2 (2021-05-15)

## 1.1.1 (2021-02-11)

### Fix

- **admin**: visits deatil view fixed

## 1.1.0 (2021-02-11)

### Feat

- **dashboard**: added history, LV and ecto

## 1.0.1 (2021-02-06)

### Fix

- **router**: fixed compillation error

## 1.0.0 (2021-02-06)

### Feat

- **docker**: enabled docker integartion

### BREAKING CHANGE

- Changes were made to enable runtime variables. Current way of deploying would not work anymore.

### Fix

- **web**: fixed tailing domain dot
- **admin**: fixed deleting urls
